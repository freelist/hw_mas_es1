classdef World
	properties
		DIM_X;
		DIM_Y;
		W;
        Attacker;
        Defender;
        nAgents;
	end
	
	methods
		function obj = World(DIM_X, DIM_Y)
			obj.DIM_X = DIM_X;
			obj.DIM_Y = DIM_Y;
			obj.W = zeros(DIM_X,DIM_Y);
        end
		
		function obj = initWorld(obj, Attacker_, Defender_)
            obj.Attacker = Attacker_;
            obj.Defender = Defender_;
            obj.nAgents = length(obj.Attacker)+length(obj.Defender);
            
 			for i = 1:length(obj.Attacker(1,:))
                obj.W(obj.Attacker(i).POS_X,obj.Attacker(i).POS_Y) = 1;
            end
  			
            for i = 1:length(obj.Defender(1,:))
                obj.W(obj.Defender(i).POS_X,obj.Defender(i).POS_Y) = 1;
            end
  			
  			assert(length(obj.W(:,1)) == obj.DIM_X,'World size changed - rows');
  			assert(length(obj.W(1,:)) == obj.DIM_Y,'World size changed - cols');
  			
  			occupiedCells = 0;
  			
  			for i = 1:length(obj.W(:,1))
  				for j = 1:length(obj.W(1,:))
  					if obj.W(i,j) == 1
  						occupiedCells = occupiedCells + 1;
  					end
  				end
  			end
  			
  			assert(occupiedCells == obj.nAgents,'%d out of 10 occupied cells (5 predators + 5 preys)',occupiedCells);
		end
		
		function Res = isFree(obj, X, Y)
			Res = (obj.W(X,Y) == 0);
        end
        
        function [obj, Defenders] = deletePrey(obj, preyId, Defenders)
            obj.nAgents = obj.nAgents - 1;
            obj.Defender(preyId).isDead = true;
            Defenders(preyId).isDead = true;
            obj.W(obj.Defender(preyId).POS_X,obj.Defender(preyId).POS_Y) = 0;
        end
        
        function printAttackerAssignments(obj, Attacker_, Defender_)
            fprintf('\n********** TASKS REACHABILITY **********\n');
            for i=1:length(Attacker_)
                display([Attacker_(i).name,' could go to tasks: [',num2str(Attacker_(i).reachableTasks),']']);
            end
            fprintf('******************************************\n');
            fprintf('\n********** ATTACKERS CONNECTIONS **********\n');
            for i=1:length(Attacker_)
                display([Attacker_(i).name,' could is connected to: [',num2str(Attacker_(i).connections),']']);
            end
            fprintf('******************************************\n');
        end
        
        function printConnectionGraph(obj, Attacker_, Defender_, Connections_)
            hold on
            for i=1:size(Connections_,1)
                for j=1:size(Connections_,2)
                    for h=1:size(Connections_,2)
                        if(h~=j)
                            if(Connections_(i,j)~=0&&Connections_(i,h)~=0)
                                %drawline
                                p1 = [obj.Attacker(Connections_(i,j)).POS_Y - ((obj.Attacker(Connections_(i,j)).POS_Y - 1) * (1 / obj.DIM_Y))+0.3,obj.Attacker(Connections_(i,j)).POS_X - ((obj.Attacker(Connections_(i,j)).POS_X - 1) * (1 / obj.DIM_X))+0.3]';
                                p2 = [obj.Attacker(Connections_(i,h)).POS_Y - ((obj.Attacker(Connections_(i,h)).POS_Y - 1) * (1 / obj.DIM_Y))+0.3,obj.Attacker(Connections_(i,h)).POS_X - ((obj.Attacker(Connections_(i,h)).POS_X - 1) * (1 / obj.DIM_X))+0.3]';
                                plot([p1(1,1);p2(1,1)],[p1(2,1);p2(2,1)],'-b','LineWidth',1);
                            end
                        end
                    end
                end
            end
        end
		
		function printWorldState(obj, Attacker_, Defender_)
            obj.Attacker = Attacker_;
            obj.Defender = Defender_;
			
			x = linspace(1,obj.DIM_Y,obj.DIM_Y + 1);
			y = linspace(1,obj.DIM_X,obj.DIM_X + 1);
			
			% Horizontal grid 
			for k = 1:length(y)
				line([x(1) x(end)], [y(k) y(k)])
			end
			
			% Vertical grid
			for k = 1:length(x)
				line([x(k) x(k)], [y(1) y(end)])
			end
			
			set(gca,'YDir','Reverse');
			set(gca,'xtick',[]);
			set(gca,'ytick',[]);
			
			for i = 1:length(obj.W(:,1))
				for j = 1:length(obj.W(1,:))
					rectangle('Position',[j - ((j - 1) * (1 / obj.DIM_Y)),i - ((i - 1) * (1 / obj.DIM_X)),(obj.DIM_Y - 1) / obj.DIM_Y,(obj.DIM_X - 1) / obj.DIM_X],'FaceColor','white')
				end
			end
			
			for i = 1:length(obj.Attacker(1,:))
				rectangle('Position',[obj.Attacker(i).POS_Y - ((obj.Attacker(i).POS_Y - 1) * (1 / obj.DIM_Y)),obj.Attacker(i).POS_X - ((obj.Attacker(i).POS_X - 1) * (1 / obj.DIM_X)),(obj.DIM_Y - 1) / obj.DIM_Y,(obj.DIM_X - 1) / obj.DIM_X],'FaceColor','red')
                text(obj.Attacker(i).POS_Y - ((obj.Attacker(i).POS_Y - 1) * (1 / obj.DIM_Y))+0.3,obj.Attacker(i).POS_X - ((obj.Attacker(i).POS_X - 1) * (1 / obj.DIM_X))+0.3, num2str(obj.Attacker(i).id))
			end
			
			for i = 1:length(obj.Defender(1,:))
                if(obj.Defender(i).isDead ~= true)
                    rectangle('Position',[obj.Defender(i).POS_Y - ((obj.Defender(i).POS_Y - 1) * (1 / obj.DIM_Y)),obj.Defender(i).POS_X - ((obj.Defender(i).POS_X - 1) * (1 / obj.DIM_X)),(obj.DIM_Y - 1) / obj.DIM_Y,(obj.DIM_X - 1) / obj.DIM_X],'FaceColor','green')
                    text(obj.Defender(i).POS_Y - ((obj.Defender(i).POS_Y - 1) * (1 / obj.DIM_Y))+0.3,obj.Defender(i).POS_X - ((obj.Defender(i).POS_X - 1) * (1 / obj.DIM_X))+0.3, num2str(obj.Defender(i).id))
                end
			end
		end
		
		function [ Res, obj ] = tryMove(obj, PREV_X, PREV_Y, NEW_X, NEW_Y)
			if ((abs(PREV_X - NEW_X) <= 1) && (abs(PREV_Y - NEW_Y) <= 1))
				Res = obj.isFree(NEW_X,NEW_Y);
				
				if Res == 1
					obj.W(PREV_X,PREV_Y) = 0;
					obj.W(NEW_X,NEW_Y) = 1;
				end
			else
				Res = 0;
			end
		end
	end
end

