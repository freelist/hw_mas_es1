function run_sample2
	DEBUG = 0;
    mDistance = 5;
	isGameEnded = 0;
    check = 0;
    taskAssigned = 0;
    while isGameEnded == 0
        %build a possible situation
        while check == 0
            [ Attacker, Defender ] = initializeAgentPositions(9,6);
            world = World(9,6);
            [ world ] = world.initWorld(Attacker,Defender);
            [check,Attacker,Defender,connections] = checkDFStree(Attacker,Defender);
        end

        if(isGameEnded==1)
            fprintf('\n********** THE GAME IS ENDED - THE DFS TREE CANNOT BE BUILT **********\n');
            break
        else
            if taskAssigned==0
                fgc = plotGraph2();
                fDfs = plotDFS2();
                pause
                assignments = computeRelations2(Attacker,Defender);
                for i=1:length(Attacker)
                    for j=1:length(assignments{i})
                        Attacker(i).tasks(end+1) = assignments{i}(j);
                        Attacker(i).taskAssigned = true;
                    end
                end
                flags = zeros(length(Defender),1);
                for i=1:length(Attacker)
                    for j=1:length(Attacker(i).tasks)
                        flags(Attacker(i).tasks(j)) = flags(Attacker(i).tasks(j)) + 1;
                    end
                end
                if ~isempty(find(flags==0))
                    fprintf('\n********** ONE TASK REMAINS NOT ASSIGNED **********\n');
                    fprintf(['Task not assigned: T', num2str(find(flags==0)),'\n']);
                    distances = zeros(length(Attacker),1);
                    for i=1:length(Attacker)
                        distances(i) = Attacker(i).getCostToTask(Defender(find(flags==0)));
                    end
                    Attacker(find(distances==min(distances))).tasks(end+1) = find(flags==0);
                    fprintf(['Task T', num2str(find(flags==0)),' has been assigned to Agent',num2str(Attacker(find(distances==min(distances))).id)]);
                    fprintf('\n***************************************************\n');
                end
                pause
                figure('name','World');
                world.printAttackerAssignments(Attacker,Defender);
                world.printWorldState(Attacker,Defender);
                world.printConnectionGraph(Attacker,Defender,connections);
                taskAssigned = 1;
                break
            else
                fprintf('\n********** THE GAME CAN START - THE DFS TREE CAN BE BUILT **********\n');
                world.printWorldState(Attacker,Defender);
            end
        end
    end
    pause
    clear all
    close all
end
