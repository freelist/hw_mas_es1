function [ P1,P2 ] = GetBorderIntersectionPoints( ls,w,h )
    %lines of image
    p11 = [[1 1] 1]';
    p12 = [[w 1] 1]';
    p21 = [[1 h] 1]';
    p22 = [[w h] 1]';
    lt = cross(p11,p12);
    lt = lt/lt(3,1);
    lb = cross(p21,p22);
    lb = lb/lb(3,1);
    ll = cross(p11,p21);
    ll = ll/ll(3,1);
    lr = cross(p12,p22);
    lr = lr/lr(3,1);
    %intersections
    Pt = cross(lt,ls);
    Pt = Pt/Pt(3,1);
    Pb = cross(lb,ls);
    Pb = Pb/Pb(3,1);
    Pl = cross(ll,ls);
    Pl = Pl/Pl(3,1);
    Pr = cross(lr,ls);
    Pr = Pr/Pr(3,1);
    P = [Pt Pb Pl Pr]';
    %filter intersections
    P_intersect = [0 0 0 0]';
    if(Pt(1,1)>=1 && Pt(1,1)<=w && Pt(2,1)>=1 && Pt(2,1)<=h)
        P_intersect(1,1) = 1;
    end
    if(Pb(1,1)>=1 && Pb(1,1)<=w && Pb(2,1)>=1 && Pb(2,1)<=h)
        P_intersect(2,1) = 1;
    end
    if(Pl(1,1)>=1 && Pl(1,1)<=w && Pl(2,1)>=1 && Pl(2,1)<=h)
        P_intersect(3,1) = 1;
    end
    if(Pr(1,1)>=1 && Pr(1,1)<=w && Pr(2,1)>=1 && Pr(2,1)<=h)
        P_intersect(4,1) = 1;
    end
    %look for the 2 intersections
    idx = find(P_intersect(:,1) == 1);
    
    if(size(idx,1) == 2)
        P1 = [P(idx(1,1),1);P(idx(1,1),2)];
        P2 = [P(idx(2,1),1);P(idx(2,1),2)];
    end


end

