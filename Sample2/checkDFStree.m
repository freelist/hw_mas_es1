function [Res,Attacker,Defender,Connections] = checkDFStree(Attacker,Defender)
    Res = 1;
    Connections = [];
    for i=1:length(Attacker)
        Attacker(i).reachableTasks = [];
    end
    %setInitial tasks
    for i = 1:length(Attacker)
        tasks = [];
        dists = [];
        for j = 1:length(Defender)
            dists(end+1) = Attacker(i).getManhattan(Defender(j).POS_X,Defender(j).POS_Y);
            if dists(end) <= 5
                tasks(end+1) = Defender(j).id;
            end
        end
        %if length(tasks)>2
        %    Attacker(i).reachableTasks = tasks(1:2);
        %elseif 
        %if isempty(tasks)
        %    Attacker(i).reachableTasks = find(dists==min(dists));
        %else
        Attacker(i).reachableTasks = tasks;
        %end
    end
    %check no tasks remain unassigned
    for i=1:length(Defender)
        flag = false;
        for j=1:length(Attacker)
            if ~isempty(find(Attacker(j).reachableTasks==Defender(i).id))
                flag = true;
                break
            end
        end
        if ~flag
            dists = [];
            for j=1:length(Attacker)
                dists(end+1) = Attacker(j).getManhattan(Defender(i).POS_X,Defender(i).POS_Y);
            end

            ctrAssignment = 1;
            flagassignment = true;
            while flagassignment == true && ctrAssignment<=length(Attacker);
                aMin = find(dists==min(dists));
                aMin = aMin(1,1);
                if length(Attacker(aMin).reachableTasks)<length(Defender)
                    Attacker(aMin).reachableTasks(end+1) = Defender(i).id;
                    flagassignment = false;
                else
                    dists(aMin) = dists(aMin) + 100;
                    ctrAssignment = ctrAssignment + 1;
                end
            end
        end
    end
    %check if the dfs tree can be built
    for i=1:length(Attacker)
        if(isempty(Attacker(i).reachableTasks))
            Res = 0;
            return
        end
    end
    tasksMatrix = zeros(length(Attacker),2);
    for i=1:length(Attacker)
        tasksMatrix(i,1:size(Attacker(i).reachableTasks,2)) = Attacker(i).reachableTasks;
    end
    for i=1:length(Defender)
        if length(find(tasksMatrix==i)) == 1
            if (find(tasksMatrix==i)-size(tasksMatrix,1))<=0
                otherAssignment = tasksMatrix(find(tasksMatrix==i)+size(tasksMatrix,1));
            else
                otherAssignment = tasksMatrix(find(tasksMatrix==i)-size(tasksMatrix,1));
            end
            if otherAssignment == 0
                Res = 0;
                return
            else
                if size(find(tasksMatrix==otherAssignment),1) == 1
                    Res = 0;
                    return
                end
            end
        end
    end
    %now print the connections in the tree
    temp_connections = [];
    for i=1:length(tasksMatrix)
        temp_connections(i,1:length(find(tasksMatrix==i))) = find(tasksMatrix==i)';
    end
    for i=1:size(temp_connections,1)
        for j=1:size(temp_connections,2)
            if(temp_connections(i,j)~=0)
                if(mod(temp_connections(i,j),length(Attacker))==0)
                    Connections(i,j) = length(Attacker);
                else
                    Connections(i,j) = mod(temp_connections(i,j),length(Attacker));
                end
            end
        end
    end
    %write nodes connections
    for i=1:length(Attacker)
        for c = 1:length(Attacker(i).reachableTasks)
            for j=1:length(Attacker)
                if(j~=i)
                    if(~isempty(find(Attacker(j).reachableTasks==Attacker(i).reachableTasks(c))) && isempty(find(Attacker(i).connections==j)))
                        Attacker(i).connections(end+1) = j;
                    end
                end
            end
        end
    end
    Res = 1;
end