function [ Attacker, Defender ] = initializeAgentPositions(MAX_X,MAX_Y)
    nAgents = 6;
    Pos = ones(nAgents,2);
    evaluation = [1 1];
    while evaluation(1,1) == 1 && evaluation(1,2) == 1
        for i=1:size(Pos,1)
            position = ones(1,2);
            while position(1,1)==1 && position(1,2)==1
                position(:,1) = randi([1 MAX_X]);
                position(:,2) = randi([1 MAX_Y]);
            end
            found = 1;
            for j=1:nAgents
                if(Pos(j,1)==position(1,1) && Pos(j,2)==position(1,2))
                    found=0;
                end
            end
            if found == 1
                Pos(i,1) = position(1,1);
                Pos(i,2) = position(1,2);
            else
                i = i-1;
            end
        end
        evaluation = Pos==ones(nAgents,2);
    end
    
    %POS_X, POS_Y,TYPE,ID,NAME,MAXMOVEMENT
	defender1 = Agent(Pos(1,1),Pos(1,2),2,1,'Task1',1,2);
	defender2 = Agent(Pos(2,1),Pos(2,2),2,2,'Task2',1,2);
	defender3 = Agent(Pos(3,1),Pos(3,2),2,3,'Task3',1,2);
	
	attacker1 = Agent(Pos(4,1),Pos(4,2),1,1,'Agent1',1,2);
	attacker2 = Agent(Pos(5,1),Pos(5,2),1,2,'Agent2',1,2);
	attacker3 = Agent(Pos(6,1),Pos(6,2),1,3,'Agent3',1,2);
	%attacker4 = Agent(Pos(7,1),Pos(7,2),1,4,'Agent4',1,2);
	%attacker5 = Agent(Pos(8,1),Pos(8,2),1,5,'Agent5',1,2);
	%attacker6 = Agent(Pos(9,1),Pos(9,2),1,6,'Agent6',1,2);
	
	%Attacker = [ attacker1, attacker2, attacker3, attacker4];
    Attacker = [ attacker1, attacker2, attacker3];
    Defender = [ defender1, defender2, defender3];
end
