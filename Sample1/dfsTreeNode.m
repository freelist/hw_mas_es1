classdef dfsTreeNode
	properties
        value
        childrens
        parents
        pseudoParents
    end
	methods
        function obj = dfsTreeNode(VALUE_, CHILDRENS_, PARENTS_, PSEUDOPARENTS_)
            obj.value = VALUE_;
            obj.childrens = CHILDRENS_;
            obj.parents = PARENTS_;
            obj.pseudoParents = PSEUDOPARENTS_;
        end
    end
end