function dfs = buildDfs(Attacker)
    %who is the root
    rootID = 1;
    connLength = length(Attacker(1).connections);
    for i=2:length(Attacker)
        if(length(Attacker(i).connections)>connLength)
            connLength = length(Attacker(i).connections);
            rootID = i;
        end
    end
    dfs = dfsTree(rootID);
    %now connects first child to the root
    secondChildId = -1;
    connLength = -1;
    for i=1:length(Attacker(rootID).connections)
        if(length(Attacker(Attacker(rootID).connections(i)).connections)>connLength)
            connLength = length(Attacker(Attacker(rootID).connections(i)).connections);
            secondChildId = Attacker(Attacker(rootID).connections(i)).id;
        end
    end
    if(secondChild~=-1)
        dfs.root.childrens{1} = dfsTree(secondChildId);
        dfs.root.childrens{1}.root.parents{1} = rootID;
        visitedNodes = [rootID secondChildId];
        %now connects first child to the first child of the root
        thirdChildId = -1;
        connLength = -1;
        for i=1:length(Attacker(secondChildId).connections)
            Attacker(Attacker(secondChildId).connections(i)).connections
            visitedNodes==Attacker(secondChildId).connections(i)
            if(length(Attacker(Attacker(secondChildId).connections(i)).connections)>connLength && isempty(find(visitedNodes==Attacker(secondChildId).connections(i))))
                connLength = length(Attacker(Attacker(secondChildId).connections(i)).connections);
                thirdChildId = Attacker(Attacker(secondChildId).connections(i)).id;
            end
        end
        if(thirdChildId~=-1)
            dfs.root.childrens{1}.root.childrens{1} = dfsTree(thirdChildId);
            dfs.root.childrens{1}.root.childrens{1}.root.parents{1} = secondChildId;
            visitedNodes = [rootID secondChildId thirdChildId];
        else
            
        end
    else
        
    end
end