function fg = plotGraph1()
    fg = figure('name','Constraint Graph','Color',[1.0 1.0 1.0]);
    hold on
    axis equal
    axis off
    %draw agents
    th = 0:pi/50:2*pi;
    xunit = 1 * cos(th) + 10;
    yunit = 1 * sin(th) + 10;
    fg = plot(xunit, yunit);
    text(10,10, 'X1');
    xunit = 1 * cos(th) + 20;
    yunit = 1 * sin(th) + 10;
    fg = plot(xunit, yunit);
    text(20,10, 'X2');
    xunit = 1 * cos(th) + 10;
    yunit = 1 * sin(th) + 0;
    fg = plot(xunit, yunit);
    text(10,0, 'X3');
    xunit = 1 * cos(th) + 20;
    yunit = 1 * sin(th) + 0;
    fg = plot(xunit, yunit);
    text(20,0, 'X4');
    %draw connections
    %X1<->X2
    plot([11;19],[10;10],'-r','LineWidth',1);
    %X1<->X4
    plot([1*cos(7*pi/4)+10;1*cos(3*pi/4)+20],[1*sin(7*pi/4)+10;1*sin(3*pi/4)+0],'-g','LineWidth',1);
    %X2<->X3
    plot([1*cos(5*pi/4)+20;1*cos(pi/4)+10],[1*sin(5*pi/4)+10;1*sin(pi/4)+0],'-b','LineWidth',1);
    %X2<->X4
    plot([20;20],[9;1],'-y','LineWidth',1);
    %X1<->X3
    plot([10;10],[9;1],'-k','LineWidth',1);
    %draw names on connections
    T12 = text(13,11, '[T1,T2,T3]');
    T12.Color = 'r';
    T24 = text(21,3, '[T1,T3]');
    T24.Color = 'y';
    set(T24, 'rotation', 90);
    T12 = text(9,5, '[T2]');
    set(T12, 'rotation', 90);
    T32 = text(12,3, '[T2]');
    set(T32, 'rotation', 45);
    T32.Color = 'b';
    T14 = text(17,4, '[T1,T3]');
    set(T14, 'rotation', -45);
    T14.Color = 'g';
    hold off
end