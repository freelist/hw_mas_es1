function drawUtilityMatrix(mat,name,projection,name2,name3)
    fg1 = figure('name',name);
    subplot(2,1,1);
    imagesc(mat);            %# Create a colored plot of the matrix values
    colormap(flipud(gray));  %# Change the colormap to gray (so higher values are
                             %#   black and lower values are white)

    textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
    textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
    [x,y] = meshgrid(1:3);   %# Create x and y coordinates for the strings
    hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                    'HorizontalAlignment','center');
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
                                                 %#   text color of the strings so
                                                 %#   they can be easily seen over
                                                 %#   the background color
    set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

    set(gca,'XTick',1:3,...                         %# Change the axes tick marks
            'XTickLabel',{'T1','T2','T3'},...  %#   and tick labels
            'YTick',1:3,...
            'YTickLabel',{'T1','T2','T3'},...
            'TickLength',[0 0]);
    subplot(2,1,2);
    % Create the column and row names in cell arrays 
    cnames = {'T1','T2','T3'};
    rnames = {name3};
    % Create the uitable
    t = uitable(fg1,'Data',projection,...
                'ColumnName',cnames,... 
                'RowName',rnames,...
                'ColumnWidth',{30});
    pos = get(subplot(2,1,2),'position');
    delete(subplot(2,1,2))
    set(t,'units','normalized')
    set(t,'position',pos)
end