function result = computeRelations1(Attackers,Tasks)
    r42 = [Attackers(4).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(3));
           Attackers(4).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(3));
           Attackers(4).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(3))];
    
    r32 = [Attackers(3).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(1))+Attackers(2).getCostToTask(Tasks(3));
           Attackers(3).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(2))+Attackers(2).getCostToTask(Tasks(3));
           Attackers(3).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(3))+Attackers(2).getCostToTask(Tasks(3))];
    
    r41 = [Attackers(4).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(4).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(4).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(1)),Attackers(4).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(2)),Attackers(4).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(3))];
    
    r31 = [Attackers(3).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(3).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(3).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(1)),Attackers(3).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(2)),Attackers(3).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(3))];
    
    r21 = [Attackers(2).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(1)),Attackers(2).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(2)),Attackers(2).getCostToTask(Tasks(1))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(2).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(1)),Attackers(2).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(2)),Attackers(2).getCostToTask(Tasks(2))+Attackers(1).getCostToTask(Tasks(3));
           Attackers(2).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(1)),Attackers(2).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(2)),Attackers(2).getCostToTask(Tasks(3))+Attackers(1).getCostToTask(Tasks(3))];
    %agent 4 cannot go to task 2
    r42(2,:) = [nan,nan,nan];
    r41(2,:) = [nan,nan,nan];
    %agent 3 cannot go to task 1 and 3
    r32(1,:) = [nan,nan,nan];
    r32(3,:) = [nan,nan,nan];
    r31(1,:) = [nan,nan,nan];
    r31(3,:) = [nan,nan,nan];
    
    %projection of 2 out of r32
    min32 = [min(r32(:,1)),min(r32(:,2)),min(r32(:,3))];
    %projection of 2 out of r42
    min42 = [min(r42(:,1)),min(r42(:,2)),min(r42(:,3))];
    %projection of 1 out of r31
    min31 = [min(r31(:,1)),min(r31(:,2)),min(r31(:,3))];
    %projection of 1 out of r41
    min41 = [min(r41(:,1)),min(r41(:,2)),min(r41(:,3))];
    
    r21(1,:) = [r21(1,1)+min32(1,1)+min42(1,1),r21(1,2)+min32(1,1)+min42(1,1),r21(1,3)+min32(1,1)+min42(1,1)];
    r21(2,:) = [r21(2,1)+min32(1,2)+min42(1,2),r21(2,2)+min32(1,2)+min42(1,2),r21(2,3)+min32(1,2)+min42(1,2)];
    r21(3,:) = [r21(3,1)+min32(1,3)+min42(1,3),r21(3,2)+min32(1,3)+min42(1,3),r21(3,3)+min32(1,3)+min42(1,3)];
    
    %projection of 2 out of r21
    min21 = [min(r21(:,1)),min(r21(:,2)),min(r21(:,3))];
    
    %draw matrices and projections
    drawUtilityMatrix(r42,'r42 - Utility Function Matrix',min42,'Projection of X2 out of r42','X2');
    drawUtilityMatrix(r41,'r41 - Utility Function Matrix',min41,'Projection of X1 out of r41','X1');
    drawUtilityMatrix(r32,'r32 - Utility Function Matrix',min32,'Projection of X2 out of r32','X2');
    drawUtilityMatrix(r31,'r31 - Utility Function Matrix',min31,'Projection of X1 out of r31','X1');
    drawUtilityMatrix(r21,'r21 - Utility Function Matrix',min21,'Projection of X1 out of r21','X1');
    
    %the choice for X1 is given by
    choiceX1 = find((min21 + min41 + min31) == min(min21 + min41 + min31));
    %the choice for X2 is given by
    choiceX2 = find(r21(:,choiceX1(1))==min(r21(:,choiceX1(1))));
    %the choice for X3 is given by
    choiceX3 = find(r32(:,choiceX2(1))==min(r32(:,choiceX2(1))));
    %the choice for X3 is given by
    choiceX4 = find(r42(:,choiceX2(1))==min(r42(:,choiceX2(1))));
    
    result = {choiceX1;choiceX2;choiceX3;choiceX4};
    
end