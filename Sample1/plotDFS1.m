function fg = plotDFS1()
    fg = figure('name','DFS tree','Color',[1.0 1.0 1.0]);
    hold on
    axis equal
    axis off
    %draw agents
    th = 0:pi/50:2*pi;
    xunit = 1 * cos(th) + 20;
    yunit = 1 * sin(th) + 20;
    fg = plot(xunit, yunit);
    text(20,20, 'X1');
    xunit = 1 * cos(th) + 20;
    yunit = 1 * sin(th) + 15;
    fg = plot(xunit, yunit);
    text(20,15, 'X2');
    xunit = 1 * cos(th) + 15;
    yunit = 1 * sin(th) + 10;
    fg = plot(xunit, yunit);
    text(15,10, 'X3');
    xunit = 1 * cos(th) + 25;
    yunit = 1 * sin(th) + 10;
    fg = plot(xunit, yunit);
    text(25,10, 'X4');
    %draw connections
    %X1->X2
    plot([20;20],[19;16],'-k','LineWidth',1);
    %X2->X3
    plot([1*cos(5*pi/4)+20;1*cos(pi/4)+15],[1*sin(5*pi/4)+15;1*sin(pi/4)+10],'-k','LineWidth',1);
    %X2->X4
    plot([1*cos(7*pi/4)+20;1*cos(3*pi/4)+25],[1*sin(7*pi/4)+15;1*sin(3*pi/4)+10],'-k','LineWidth',1);
    %X3->X1
    plot([1*cos(pi/4)+15;1*cos(5*pi/4)+20],[1*sin(pi/4)+10;1*sin(5*pi/4)+20],'--k','LineWidth',1);
    %X4->X1
    plot([1*cos(3*pi/4)+25;1*cos(7*pi/4)+20],[1*sin(3*pi/4)+10;1*sin(7*pi/4)+20],'--k','LineWidth',1);
    %draw names on connections
    T32 = text(18,12, 'r32');
    T32 = text(21,12, 'r42');
    T21 = text(20.25,17, 'r21');
    T41 = text(23,15, 'r41');
    T31 = text(16,15, 'r31');
end