function [ Attacker, Defender ] = initializeAgentPositions(MAX_X,MAX_Y)
  %POS_X, POS_Y,TYPE,ID,NAME,MAXMOVEMENT
	defender1 = Agent(2,5,2,1,'Task1',1,2);
	defender2 = Agent(5,6,2,2,'Task2',1,2);
	defender3 = Agent(1,3,2,3,'Task3',1,2);

	attacker1 = Agent(1,6,1,1,'Agent1',1,2);
	attacker2 = Agent(1,5,1,2,'Agent2',1,2);
	attacker3 = Agent(8,6,1,3,'Agent3',1,2);
	attacker4 = Agent(1,1,1,4,'Agent4',1,2);

	Attacker = [ attacker1, attacker2, attacker3, attacker4];
    %Attacker = [ attacker1, attacker2, attacker3];
    Defender = [ defender1, defender2, defender3];
end
