function run_sample1
	DEBUG = 0;
    mDistance = 5;    
	isGameEnded = 0;
    check = 0;
    taskAssigned = 0;
    while isGameEnded == 0
        %build a possible situation
        while check == 0
            [ Attacker, Defender ] = initializeAgentPositions(9,6);
            world = World(9,6);
            [ world ] = world.initWorld(Attacker,Defender);
            [check,Attacker,Defender,connections] = checkDFStree(Attacker,Defender);
        end
        
        if(isGameEnded==1)
            fprintf('\n********** THE GAME IS ENDED - THE DFS TREE CANNOT BE BUILT **********\n');
            break
        else
            if taskAssigned==0
                fgc = plotGraph1();
                fDfs = plotDFS1();
                pause
                assignments = computeRelations1(Attacker,Defender);
                for i=1:length(Attacker)
                    for j=1:length(assignments{i})
                        Attacker(i).tasks(end+1) = assignments{i}(j);
                        Attacker(i).taskAssigned = true;
                    end
                end
                pause
                figure('name','World');
                world.printAttackerAssignments(Attacker,Defender);
                world.printWorldState(Attacker,Defender);
                world.printConnectionGraph(Attacker,Defender,connections);
                taskAssigned = 1;
                break
            else
                fprintf('\n********** THE GAME CAN START - THE DFS TREE CAN BE BUILT **********\n');
                world.printWorldState(Attacker,Defender);
            end
        end
    end
    pause
    clear all
    close all
end