function main_tracking
	DEBUG = 0;
    mDistance = 5;
	
	%[ Attacker, Defender ] = initializeAgentPositions(9,6);
	
	%world = World(9,6);
	
	%[ world ] = world.initWorld(Attacker,Defender);
	
	figure('name','World');
	
	%world.printWorldState(Attacker,Defender);
	%if DEBUG == 1
	%	fprintf('\n');
    %end
    
	isGameEnded = 0;
    check = 0;
    while isGameEnded == 0
        %build a possible situation
        while check == 0
            [ Attacker, Defender ] = initializeAgentPositions(9,6);
            world = World(9,6);
            [ world ] = world.initWorld(Attacker,Defender);
            [check,Attacker,Defender,connections] = checkDFStree(Attacker,Defender);
        end
        %dfsTree = buildDfs(Attacker);
        world.printWorldState(Attacker,Defender);
        world.printConnectionGraph(Attacker,Defender,connections);
        
        if(isGameEnded==1)
            fprintf('\n********** THE GAME IS ENDED - THE DFS TREE CANNOT BE BUILT **********\n');
            break
        else
            fprintf('\n********** THE GAME CAN START - THE DFS TREE CAN BE BUILT **********\n');
            world.printAttackerAssignments(Attacker,Defender);
            break
            for i = 1:length(Attacker(1,:))

                r_x = 100;
                r_y = 100;
                while Attacker(i).getManhattan(r_x,r_y)~=1
                    r_x = randi(world.DIM_X);
                    r_y = randi(world.DIM_Y);
                end			
                [ ~, Attacker(i), world ] = Attacker(i).move(world,r_x,r_y);

            end

            if DEBUG == 1
                fprintf('\n');
            end

            %for i = 1:length(Defender(1,:))
            %	r_x = randi(world.DIM_X);
            %	r_y = randi(world.DIM_Y);
            %	
            %	[ ~, Defender(i), world ] = Defender(i).move(world,r_x,r_y);
            %	
            %	if DEBUG == 1
            %		fprintf('Defender %d is now in state: (%d, %d)\n', i, Defender(i).POS_X, Defender(i).POS_Y);
            %	end
            %end

            if DEBUG == 1
                fprintf('\n');
            end

            world.printWorldState(Attacker,Defender);

            if DEBUG == 1
                fprintf('\n');
            end

            pause
        end
    end
    pause
    clear all
    close all
end
