classdef Agent
	properties
        POS_X
		POS_Y
        maxMovement
        maxTaskAssigned
        type
        id
        name
        isDead
        reachableTasks
        taskAssigned
        tasks
        connections
        dfsConnections
	end
	
	methods
		function obj = Agent(POS_X, POS_Y,TYPE,ID,NAME,MAXMOVEMENT,MAXTASKASS)
			obj.POS_X = POS_X;
			obj.POS_Y = POS_Y;
            obj.maxMovement = MAXMOVEMENT;
            obj.type = TYPE;
            obj.id = ID;
            obj.name = NAME;
            obj.isDead = false;
            obj.maxTaskAssigned = MAXTASKASS;
            obj.tasks = [];
            obj.reachableTasks = [];
            obj.connections = [];
            obj.dfsConnections = [];
            obj.taskAssigned = false;
        end
        
        function [dist] = getManhattan(obj,X,Y)
            dist = abs(obj.POS_X - X) + abs(obj.POS_Y - Y);
        end
        
        function [cost] = getCostToTask(obj, task)
            cost = 0;
            if(obj.POS_X - task.POS_X)>0
                %going North costs 3
                cost = cost + abs(obj.POS_X - task.POS_X) * 3;
            else
                %going South costs 2
                cost = cost + abs(obj.POS_X - task.POS_X) * 2;
            end
            %going West or East costs 1
            cost = cost + abs(obj.POS_Y - task.POS_Y) * 1;
        end
		
		function [ Res, obj, worldObj ] = move(obj, worldObj, X, Y)
			[ Res, worldObj ] = worldObj.tryMove(obj.POS_X,obj.POS_Y,X,Y);
			
			if Res == 1
				obj.POS_X = X;
				obj.POS_Y = Y;
			end
		end
	end
end
