classdef dfsTree
	properties
        root
    end
	methods
        function obj = dfsTree(rootValue)
            obj.root = dfsTreeNode(rootValue,{},{},{});
        end
        function nodeList = postOrderVisit(obj)
            nodeList = [];
            if(isempty(obj.root.childrens))
                %display(num2str(node.value));
                nodeList(end+1) = obj.root.value;
            else
                for i=1:length(obj.root.childrens)
                    nodeList2 = obj.root.childrens{i}.postOrderVisit();
                    nodeList = [nodeList;nodeList2];
                end
                %display(num2str(node.value));
                nodeList(end+1) = obj.root.value;
            end
        end
    end
end